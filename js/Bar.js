document.getElementById('btnBuscar').addEventListener('click', function() {
    const cocktailName = document.getElementById('cocktail-name').value;

    if (cocktailName) {
        axios.get(`https://www.thecocktaildb.com/api/json/v1/1/search.php?s=${cocktailName}`)
        .then(function(response) {
            const cocktail = response.data.drinks.filter(bebida => bebida.strDrink.toUpperCase() === cocktailName.toUpperCase());
            if (cocktail.length === 1) {
                document.getElementById('cocktail-category').value = cocktail[0].strCategory;
                document.getElementById('cocktail-instructions').value = cocktail[0].strInstructions;
                document.getElementById('selected-image').src = cocktail[0].strDrinkThumb;
            } else {
                alert('No se encontró ninguna bebida con ese nombre.');
                limpiarCampos();
            }
        })
        .catch(function(error) {
            alert('No se encontró ninguna bebida con ese nombre.');
            limpiarCampos();
            console.log(error);
        });
    } else {
        alert('Por favor, ingresa un nombre de bebida.');
    }
});

function limpiarCampos() {
    document.getElementById('cocktail-name').value = '';
    document.getElementById('cocktail-category').value = '';
    document.getElementById('cocktail-instructions').value = '';
    document.getElementById('selected-image').src = '';
}

document.getElementById('clear-button').addEventListener('click', limpiarCampos);
